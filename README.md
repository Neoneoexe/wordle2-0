# Wordle 2.0

Inspired by [Wordle](https://www.nytimes.com/games/wordle/index.html) I made my own clone.

- NO limits on the words.

- the words that are generated are not the same as the real game.

### Tech

- NextJS
- React
- TypeScript


### Want a new feature?

- [ Upcoming feature ] 
- A battle wordle with your friend to see who guesses first, implementation with socket.io


### Live Site 

Deployed site at: [Available here]()


### BUGS:

- None at the time being

Submit a issue [here]() if you spot any bugs.
